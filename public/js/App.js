CA = {
  resizeTimer: false,
  init: function (windowLoaded) {

    if ( windowLoaded === undefined ) {

      CA.svgCheck();
      CA.localAnchors();
      CA.fixColumns();
      $(window).load(function(){
        CA.init(true);
      })
    } else {
      CA.stickyNav();
      CA.mobileNav();
    }

  },
  svgCheck: function () {
    if (Modernizr.svg) {
      return;
    }
    $('img[src*="svg"]').attr('src', function () {
      return $(this).attr('src').replace('.svg', '.png');
    });
  },
  localAnchors: function () {
    $('a').each(function () {
      var href = $(this).attr('href');
      if ( href === undefined )
      {
        return;
      }

      if ( href.length == 2 && href.substr(0,2) == '/#' && document.location.href == '/' ) {
        //just catch this and ignore
      } else if (href.length == 1 || href.substr(0, 1) != '#') {
        return;
      }
      $(this).click(function (e) {
        e.preventDefault();
        $('html, body').animate({scrollTop: $(href).position().top + 1}, 'slow');
      });
    })
  },
  stickyNav: function () {
    $(window).scroll(function(){
      var scroll = $(window).scrollTop();

      if (scroll >= 50) {
        $("header").addClass("sticky");
      } else {
        $("header").removeClass("sticky");
      }
      console.log(scroll);
    });
  },
  mobileNavStartHeight: false,
  mobileNavEndHeight: false,
  mobileNav: function () {

    var header = $('header');
    if (! CA.mobileNavEndHeight ) {
      CA.mobileNavEndHeight = parseInt($(header).height(), 10) +
      parseInt($(header).find('nav').height(), 10) + 60;
    }
    if (! CA.mobileNavStartHeight ) {
      CA.mobileNavStartHeight = parseInt($(header).height(), 10) + 40;
    }
    $('#mobile-nav').click(function(e){
      e.preventDefault();

      if ( $(this).data('open' ) ) {
        $(this).data('open', false);
        $('header').stop().animate({height: CA.mobileNavStartHeight + 'px'});
        $('body').stop().animate({paddingTop: CA.mobileNavStartHeight + 'px'});
      } else {
        $(this).data('open', true);
        $('header').stop().animate({height: CA.mobileNavEndHeight + 'px'});
        $('body').stop().animate({paddingTop: CA.mobileNavEndHeight + 'px'});
      }

    }).data('open', false);
  },
  fixColumns: function (windowLoaded, resetFixed ) {
    $('.columnList').each(function () {
      var list = $(this);
      if ( resetFixed ) {
        $(list).data('fixed', false);
      }
      if ( $(list).data('fixed')) {
        return;
      }
      var hasImages = $(list).find('img').size() > 0;
      //If we have images in this list we need to wait for window.load
      if ( windowLoaded === undefined &&  hasImages ) {
        return;
      }

      if ( hasImages ) {
        var maxImageHeight = 0;
        $(list).find('.imageWrap').each(function() {
          if ( $(this).height() > maxImageHeight ) maxImageHeight = $(this).height();
        });
        $(list).find('.imageWrap').height(maxImageHeight);
        $(list).find('img').each(function() {
          var imgHeight = $(this).height();
          var margin = Math.round((maxImageHeight - imgHeight)/2);
          $(this).css({marginTop: margin+'px'});

        });
      }

        var strongHeight = 0;

        $(list)
            .find('strong').each(function(){
                var height = $(this).innerHeight();
                if ( height > strongHeight )
                    strongHeight = height;
            }
        )


        if ( strongHeight === null ) strongHeight = 0;

      //We only support three for four columns
      var columns = $(list).hasClass('column_3') ? 3 : 4;
      var height = maxHeight = 0;
      var i = 0;
      $(list).children('li').each(function () {
        $(this).css({'height': 'auto'});
        i++;
        height = parseInt($(this).height(), 10);
        if (height > maxHeight) maxHeight = height;
        if (i % columns == 0) {
          $(list).children('li').slice(i - columns, i).css({height: (maxHeight+strongHeight) + 'px'});
        }
      });



      $(list)
        .find('strong').css({
          position: 'absolute',
          bottom: '0px'
        })
        .data('fixed', true);

    });

    //Finally - call this again once the everything has loaded
    $(window).load(function(){
      CA.fixColumns(true);
    })
    .resize(function(){
        clearTimeout(CA.resizeTimer);
        CA.resizeTimer = setTimeout(function(){
          CA.fixColumns(true, true);
        }, 500);
    });

  }
}

$(document).ready(function () {
  CA.init();
})