<?php

namespace CR\Models\Panels;

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Illuminate\View\View;

class MapPanel extends BasePanel
{

  use SoftDeletingTrait;

  protected $_type = 'MapPanel';


  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'panels_maps';

  protected $fillable = [
    'uniqueId',
    'class_names',
    'enabled',
    'global'
  ];

  protected $guarded = ['id'];

  protected $dates = ['deleted_at'];

}
