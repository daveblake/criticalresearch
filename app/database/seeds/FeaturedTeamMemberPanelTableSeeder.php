<?php

class FeaturedTeamMemberPanelTableSeeder extends Seeder
{

  public function run()
  {
    $panels = [
      [
        'uniqueId'    => 'featuredTeam',
        'class_names' => 'greyBackground',
        'enabled'     => 1,
        'global'      => 1
      ]
    ];

    DB::table('panels_featured_team_members')->truncate();
    foreach($panels as $panel)
    {
      $panel['uniqueId'] = uniqid();
      \CR\Models\Panels\FeaturedTeamMemberPanel::create($panel);
    }
  }
}

