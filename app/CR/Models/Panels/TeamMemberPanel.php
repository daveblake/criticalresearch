<?php

namespace CR\Models\Panels;

use CR\Models\TeamMember;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Illuminate\View\View;

class TeamMemberPanel extends BasePanel
{

  use SoftDeletingTrait;

  protected $_type = 'TeamMemberPanel';


  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'panels_featured_team_members';

  protected $fillable = [
    'uniqueId',
    'class_names',
    'enabled',
    'global'
  ];

  protected $guarded = ['id'];

  protected $dates = ['deleted_at'];

  protected $_teamMember;

  public function render()
  {
    $View = parent::render();
    return $View->with('TeamMember', $this->teamMember());
  }

  public function setTeamMember(TeamMember $teamMember) {
    $this->_teamMember = $teamMember;
    return $this;
  }

  public function getTeamMember()
  {
    return $this->_teamMember;
  }

  public function teamMember() {
    return $this->getTeamMember();
  }
}
