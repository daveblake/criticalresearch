<section id="{{{$Panel->uniqueId}}}"
         class="panel featuredTeamMember {{$Panel->class_names}}">

  <div class="content">

    <blockquote>{{$TeamMember->quote}}</blockquote>

    <p>
      <strong>{{$TeamMember->name}}</strong><br />
      {{$TeamMember->job_title}}
    </p>

    <a href="./team/{{$TeamMember->slug}}"
       class="button">Meet {{$TeamMember->firstName()}}</a>

    <img class="main" src="/img/team/{{{$TeamMember->image_name}}}"
         alt="{{$TeamMember->name}}" />
  </div>
</section>