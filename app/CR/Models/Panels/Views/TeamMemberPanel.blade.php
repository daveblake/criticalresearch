<section id="{{{$Panel->uniqueId}}}"
         class="panel teamMember {{{$Panel->class_names}}}">

  <div class="content">

    <h3>
      <strong>{{$TeamMember->name}}</strong><br/>
      {{$TeamMember->job_title}}</h3>

    <p class="description">{{{$TeamMember->description}}}</p>

    <a href="/team"
       class="button">Back</a>

    <img class="main" src="/img/team/{{{$TeamMember->image_name}}}"
         alt="{{{$TeamMember->name}}}"/>
  </div>
</section>