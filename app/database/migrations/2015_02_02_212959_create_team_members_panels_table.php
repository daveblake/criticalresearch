<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamMembersPanelsTable extends Migration {

	public static $table = 'panels_team_members';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!\Schema::hasTable(static::$table)) {
			\Schema::create(
				static::$table,
				function ($table) {

					$table->increments('id')->unsigned();
					$table->string('uniqueId', 200)->unique();

					$table->string('class_names', 200);

					$table->boolean('enabled')->default(1);
					$table->boolean('global')->default(0);

					$table->timestamps();
					$table->softDeletes();

					$table->index('enabled');
					$table->index('global');
				}
			);
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\Schema::dropIfExists(static::$table);
	}

}
