<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>{{{$Page->seo('title')}}} - Critical Research</title>
  <meta name="description" content="{{{$Page->seo('description')}}} - Critical Research">
  <meta name="keywords" content="{{{$Page->seo('keywords')}}} - Critical Research">
  <meta name="viewport" content="width=device-width, initial-scale=1">


  <link rel="stylesheet" href="/css/normalize.min.css">

  <script src="/js/vendor/modernizr-2.6.2.min.js"></script>
  <link rel="stylesheet" href="/css/styles.css">
</head>
<body class="{{{$bodyClass}}}">
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
  your browser</a> to improve your experience.</p>
<![endif]-->


@yield('content')


<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.1.min.js"><\/script>')</script>
<script src="http://malsup.github.com/jquery.cycle2.js"></script>
<script src="/js/App.js"></script>

</body>
</html>
