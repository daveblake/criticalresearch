<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		if ( in_array('--with-user', $_SERVER['argv'])) {
			$this->call('DevUserTableSeeder');
			$this->command->info('Dev User table seeded!');
		} else {
			$this->command->info('------------------------');
			$this->command->info('Dev User table NOT seeded!');
			$this->command->info('Seed the users by adding the --with-user option');
			$this->command->info('------------------------');
		}


		$this->call('PageTableSeeder');
		$this->command->info('Page table seeded!');

		$this->call('FullscreenPanelTableSeeder');
		$this->command->info('Fullscreen Panels table seeded!');

		$this->call('PagePanelsTableSeeder');
		$this->command->info('Page Panels table seeded!');

		$this->call('QuotePanelTableSeeder');
		$this->command->info('Quote Panels table seeded!');

		$this->call('MapPanelTableSeeder');
		$this->command->info('Map Panels table seeded!');

		$this->call('ColumnPanelTableSeeder');
		$this->command->info('Column Panels table seeded!');

		$this->call('TeamMemberTableSeeder');
		$this->command->info('Team Member table seeded!');

		$this->call('FeaturedTeamMemberPanelTableSeeder');
		$this->command->info('Featured Team Member Panels table seeded!');

		$this->call('TeamMembersPanelTableSeeder');
		$this->command->info('Team Members Panels table seeded!');
	}

}
