<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTextPanelsTable extends Migration {

	public static $table = 'panels_text';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!\Schema::hasTable(static::$table)) {
			\Schema::create(
				static::$table,
				function ($table) {

                    $table->increments('id')->unsigned();


                    $table->string('title', 200);
                    $table->longtext('content');

                    $table->integer('weight')->unsigned();


                    $table->timestamps();
                    $table->softDeletes();

                    $table->index('weight');
				}
			);
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\Schema::dropIfExists(static::$table);
	}

}
