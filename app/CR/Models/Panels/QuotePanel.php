<?php

namespace CR\Models\Panels;

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Illuminate\View\View;

class QuotePanel extends BasePanel
{

  use SoftDeletingTrait;

  protected $_type = 'QuotePanel';


  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'panels_quotes';

  protected $fillable = [
    'uniqueId',
    'quote',
    'quote_by',
    'button_link',
    'button_text',
    'image_path',
    'image_alt',
    'class_names',
    'enabled',
    'global'
  ];

  protected $guarded = ['id'];

  protected $dates = ['deleted_at'];

}
