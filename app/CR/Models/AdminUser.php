<?php

namespace CR\Models;

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;

class AdminUser extends \Eloquent implements UserInterface
{

  use UserTrait;

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'users';

  protected $fillable = array('username', 'email');

  protected $guarded = array('id', 'password');

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = array('password', 'remember_token');

  public function setPasswordAttribute($password)
  {
    $this->attributes['password'] = \Hash::make($password);
  }
}
