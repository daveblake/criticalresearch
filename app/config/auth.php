<?php

return array(
  'multi' => array(
    'dev' => array(
      'driver' => 'eloquent',
      'model' => '\CR\Models\DevUser'
    ),
    'admin' => array(
      'driver' => 'eloquent',
      'model' => '\CR\Models\AdminUser'
    )
  ),
  'reminder' => array(

    'email' => 'emails.auth.reminder',
    'table' => 'password_reminders',
    'expire' => 60,

  ),

);
