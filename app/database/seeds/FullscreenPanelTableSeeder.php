<?php

class FullscreenPanelTableSeeder extends Seeder
{

  public function run()
  {
    $panels = [
      [
        'title' => 'Over The Top?',
        'strapline' => 'Possibly, but we do like to go the extra mile.',
        'content' => '<p>Nothing fishy here. It\'s simply a question of applying creative thinking to all areas of market
          research. Of specialising in telephone, online and qualitative research. Of working closely with
          you. <span class="mobile-hide">Of using small, specialised teams. And, interestingly, of caring. Caring about clients. Caring
          about our bright young things, and our work. Passionately.</span>
        </p>
        <p><span class="mobile-hide">
  It means we fish harder to give you clear, easy-to-understand results.<br>
  That\'s absolutely Critical.</span>
        </p>',
        'button_link' => '/what-we-do',
        'button_text' => 'Work With Us',
        'image_path' => '/img/panels/boy-fish.png',
        'image_alt' => 'Boy and Fish Illustration',
        'class_names' => 'blueBackground',
        'enabled' => 1,
        'global' => 0
      ],
      [
        'uniqueId' => 'magnificent',
        'title' => 'Magnificent',
        'strapline' => 'Actually, yes.',
        'content' => '
        <p>Caring means our clients come back to us again and again. And our bright young things stay with us to
          grow in magnitude; to become highly experienced researchers. <span class="mobile-hide">It means we offer everything from
          telephone and online research to advanced statistical and analytical skills. It means we regularly
          provide combined qualitative and quantitative solutions.</span>
        </p>

        <p>
          <span class="mobile-hide">So we\'re able to present you with...genuine insight. Something we’re always happy to expand upon.</span>
        </p>',
        'button_link' => '/our-work',
        'button_text' => 'How We Work',
        'image_path' => '/img/panels/boy-magnify.png',
        'image_alt' => 'Boy and Magnifying Glass Illustration',
        'class_names' => 'white',
        'enabled' => 1,
        'global' => 0
      ],
      [
        'uniqueId' => 'brightYoungThings',
        'title' => 'International Bright Young Things.',
        'strapline' => 'Our people are the most Critcal thing about us.',
        'content' => '
        <p>Better understanding is a result of knowing people. So it\'s Critical to form relationships
          based on trust. Trust between respondents and interviewers, and project managers and clients.
        </p>

        <p><span class="mobile-hide">We\'re naming the faces at Critical. To get to know us better,
          simply click on the names in the menu or talk to one of us.</span>
        </p>',
        'button_link' => '/team',
        'button_text' => 'Meet The Team',
        'image_path' => '/img/panels/rabbit-hat.png',
        'image_alt' => 'Rabbit in a Hat',
        'class_names' => 'greyBackground',
        'enabled' => 1,
        'global' => 0
      ],
      [
        'uniqueId' => 'biggerInsights',
        'title' => 'We Always Go Further To Gain Bigger Insights From Market Research.',
        'strapline' => 'Without over-magnifying the point; we care. Caring is part of going further.',
        'content' => '',
        'button_link' => '#read_on',
        'button_text' => 'Read On',
        'image_path' => '/img/panels/boy-magnify.png',
        'image_alt' => 'Boy and Magnifying Glass Illustration',
        'class_names' => 'white biggerInsights',
        'enabled' => 1,
        'global' => 0
      ],
      [
        'uniqueId' => 'trifleHarsh',
        'title' => 'A Trifle Harsh?',
        'strapline' => 'Possibly, but we do go further for our Media clients',
        'content' => '<p>Clear, unequivocal insight is Critical.
So we go further to offer a more comprehensive range of innovative
media research services.</p>
          <p><span class="mobile-hide">The result is a better understanding of your target audience.
          Clearer tracking to help maximise your ROI. More defined New Media evaluation
          and positioning. Better readership and usage measurement.
          More accurate broadcast media, online and channel performance tracking.</span></p>',
        'button_link' => '/#contact',
        'button_text' => 'Talk To Us',
        'image_path' => '/img/panels/man-laser.png',
        'image_alt' => 'A laser is about to cut a man in half, James Bond?',
        'class_names' => 'pink whiteBackground borderTop',
        'enabled' => 1,
        'global' => 0
      ],
      [
        'uniqueId' => 'anoraks',
        'title' => 'Anoraks?',
        'strapline' => 'Certainly not. Just passionate about collecting high-quality data.',
        'content' => '<p>We specialise in high quality data collection. Not to mention telephone and on-line research.
Advanced in-house DP. Impeccable client service. Flexibility.
The ability to meet even the most demanding timelines, plus in-depth experience in both business and consumer research.
<span class="mobile-hide">We also have the capacity to handle all types of research, from large,
continuous projects to smaller ad-hoc surveys.</span></p>
<p>
<span class="mobile-hide">
We\'ll use all our bottled experience to deliver high quality, cost effective research. On time and within budget.</span></p>',
        'button_link' => '/#contact',
        'button_text' => 'Talk To Us',
        'image_path' => '/img/panels/jars.png',
        'image_alt' => 'Jars of data',
        'class_names' => 'green whiteBackground borderTop',
        'enabled' => 1,
        'global' => 0
      ],
      [
        'uniqueId' => 'tooIntense',
        'title' => 'Too Intense?',
        'strapline' => 'Possibly, but we do go further for our Media clients',
        'content' => '<p>By providing a comprehensive range of financial research service sectors.
By creating innovative solutions. By using our considerable experience. <span class="mobile-hide">We’re able to design and implement
key strategic initiatives for leading banks, building societies, insurance companies and financial institutions.</span></p>
<p><span class="mobile-hide">Initiatives like customer service, satisfaction and retention programmes. Like brand positioning.
E-commerce. IFA segmentation. NPD. We even illuminate the more murky areas
like customer understanding and compliance.</span></p>',
        'button_link' => '/#contact',
        'button_text' => 'Talk To Us',
        'image_path' => '/img/panels/light-on-chair.png',
        'image_alt' => 'Light shining on a chair',
        'class_names' => 'purple whiteBackground borderTop',
        'enabled' => 1,
        'global' => 0
      ],
      [
        'uniqueId' => 'offTheShelf',
        'title' => 'Off The Shelf?',
        'strapline' => 'Not on your nelly. We only offer tailored online surveys',
        'content' => '<p>We specialise in high quality data collection. Not to mention telephone and on-line research.
Advanced in-house DP. Impeccable client service. Flexibility. <span class="mobile-hide">The ability to meet even the most demanding timelines,
plus in-depth experience in both business and consumer research. We also have the capacity to handle all types of
research, from large, continuous projects to smaller ad-hoc surveys.</span></p>
<p><span class="mobile-hide">We\'ll use all our bottled experience to deliver high quality, cost effective research.
On time and within budget.</span> </p>',
        'button_link' => '/#contact',
        'button_text' => 'Talk To Us',
        'image_path' => '/img/panels/elephants-ballet.png',
        'image_alt' => 'An elephant doing ballet',
        'class_names' => 'orange  whiteBackground borderTop',
        'enabled' => 1,
        'global' => 0
      ],
      [
        'uniqueId' => 'ourWork',
        'title' => 'The Best? Better Ask Our Clients.',
        'strapline' => 'In research, the more you listen the more you learn.',
        'content' => '<p>We don’t do \'them and us\'. For Critical results, it’s important that you, our research
team and your respondents form one, unbroken line of communication. Linked by our research team.</p>',
        'button_link' => '#testimonials',
        'button_text' => 'What Our Clients Say',
        'image_path' => '/img/panels/bumble-bee-man.png',
        'image_alt' => 'A man dressed as a bumble-bee',
        'class_names' => 'greenBackground',
        'enabled' => 1,
        'global' => 0
      ]
    ];


    DB::table('panels_fullscreen')->truncate();
    foreach ($panels as $panel) {
      if (! isset($panel['uniqueId']) )
        $panel['uniqueId'] = uniqid();
      \CR\Models\Panels\FullscreenPanel::create($panel);
    }

  }
}

