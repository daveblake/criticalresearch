<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    @if (Auth::admin()->check())
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed"
                data-toggle="collapse" data-target="#navbar"
                aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">Critical Research</a>
      </div>
      <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-right">
          @foreach( $navigation as $link )
            <li @if($link['active'])class="active"@endif>
              <a href="{{$link['href']}}">{{{$link['name']}}}
                @if($link['active'])<span class="sr-only">(current)</span>@endif
              </a>
            </li>
          @endforeach
            <li><a href="/logout">Logout</a></li>
         </ul>
      </div>
    @else
      <div class="navbar-header">
        <a class="navbar-brand" href="/">Critical Research</a>
      </div>
    @endif
  </div>
</nav>