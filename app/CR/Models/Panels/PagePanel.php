<?php

namespace CR\Models\Panels;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class PagePanel extends \Eloquent
{

  use SoftDeletingTrait;

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'page_panels';

  protected $fillable = [
    'page_id',
    'panel_id',
    'panel_type',
    'weight'
  ];

  protected $guarded = ['id'];

  protected $dates = ['deleted_at'];

}
