<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColumnPanelsTable extends Migration {

	public static $table = 'panels_columns';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!\Schema::hasTable(static::$table)) {
			\Schema::create(
				static::$table,
				function ($table) {
					$table->increments('id')->unsigned();
					$table->string('uniqueId', 200)->unique();
					$table->enum('column_type', ['column_3','column_4'])->default('column_3');

					$table->string('class_names', 200);

					$table->string('title', 200);
					$table->string('button_link', 100);
					$table->string('button_text', 50);
					$table->string('button_qualifier', 100);

					$table->boolean('enabled')->default(1);
					$table->boolean('global')->default(0);

					$table->timestamps();
					$table->softDeletes();

					$table->index('enabled');
					$table->index('global');

				}
			);
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\Schema::dropIfExists(static::$table);
	}

}
