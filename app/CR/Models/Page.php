<?php

namespace CR\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Page extends \Eloquent
{

  use SoftDeletingTrait;

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'pages';

  protected $fillable = [
    'parent_id',
    'slug',
    'title',
    'enabled',
    'weight',
    'seo',
    'meta_title',
    'meta_keywords',
    'meta_description'
  ];

  protected $guarded = ['id'];

  protected $dates = ['deleted_at'];

  protected $_panelCollection;

  public function fromSlugArray(array $slugArray)
  {
    if(empty($slugArray))
    {
      $slugArray = ['index'];
    }

    $parentId = 0;
    foreach($slugArray as $slug)
    {
      $page     = self::where('parent_id', $parentId)
        ->where('slug', $slug)
        ->firstOrFail();
      $parentId = $page->parent_id;
    }
    return $page;
  }

  public function seo($seoType)
  {
    switch($seoType)
    {
      case 'title':
        return $this->meta_title ?: $this->title;
        break;
      case 'keywords':
        return $this->meta_keywords ?: $this->title;
        break;
      case 'description':
        return $this->meta_description ?: $this->title;
        break;
      default:
        throw new \Exception('Unknown SEO type');
    }
  }

  public function panels()
  {
    if ( is_null($this->_panelCollection) )
    {
      $this->loadPanels();
    }

    return $this->_panelCollection;
  }

  protected function loadPanels()
  {
    $panelNamespace  = 'CR\Models\Panels\\';
    $this->_panelCollection = new Collection();
    $prevPanelReadOn = false;
    foreach($this->hasMany('CR\Models\Panels\PagePanel')->get() as $PagePanel)
    {
      $className = $panelNamespace . $PagePanel->panel_type;

      $Panel = (new $className)->find($PagePanel->panel_id);
      //If the previous panel has a #read_on link then update the ID with the ID from this panel.
      if($prevPanelReadOn)
      {
        $this->_panelCollection->last()->button_link = '#' . $Panel->uniqueId;
        $prevPanelReadOn                      = false;
      }
      //If we have a #read_on link then set flag.
      if(isset($Panel->button_link) && $Panel->button_link == '#read_on')
      {
        $prevPanelReadOn = true;
      }
      $Panel->setPage($this);
      $this->_panelCollection->add($Panel);
    }
    return $this->_panelCollection;
  }

  public function navColor()
  {
    $navColor = 'blue';
    if(count($this->panels()) == 0)
    {
      return $navColor;
    }
    $classes = explode(' ', $this->panels()->first()->class_names);
    if(count($classes) == 0)
    {
      return $navColor;
    }

    switch(true)
    {
      case in_array('greenBackground', $classes):
      case in_array('blueBackground', $classes):
        $navColor = 'white';
        break;
    }

    return $navColor;
  }

}
