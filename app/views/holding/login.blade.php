@section('content')

    <h1><img alt="Critical Research" src="img/logo.svg" class="svg" /></h1>

    <p>Dev Login</p>

    <form method="post" action="/login">
        <div class="box
    <?= ( $errors->first('email') ) ? 'error' : ''; ?>">
            <label for="email">Email</label>
            <input type="text"
                   name="email"
                   id="email"
                   value="{{{$email}}}"
                    />
            <?php if ( $errors->first('email') ): ?>
            <p class="error">{{{$errors->first('email')}}}</p>
            <?php endif; ?>
        </div>
        <div class="box
    <?= ( $errors->first('password') ) ? 'error' : ''; ?>">
            <label for="password">Password</label>
            <input type="password"
                   name="password"
                   id="password"
                   value=""
                    />
            <input class="submit" type="submit" value="Go" />
            <?php if ( $errors->first('password') ): ?>
            <p class="error">{{{$errors->first('password')}}}</p>
            <?php endif; ?>
        </div>

    </form>

@stop
