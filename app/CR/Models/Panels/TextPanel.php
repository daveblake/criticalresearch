<?php

namespace CR\Models\Panels;

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Illuminate\View\View;

class TextPanel extends BasePanel
{

  use SoftDeletingTrait;

  protected $_type = 'TextPanel';


  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'panels_text';

  protected $fillable = [
    'title',
    'content',
  ];

  protected $guarded = ['id'];

  protected $dates = ['deleted_at'];

}
