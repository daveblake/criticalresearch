<?php

class MapPanelTableSeeder extends Seeder
{

  public function run()
  {
    $panels = [
      [
        'class_names' => '',
        'enabled' => 1,
        'global' => 0
      ]
    ];


    DB::table('panels_maps')->truncate();
    foreach ($panels as $panel) {
      $panel['uniqueId'] = 'contact';
      \CR\Models\Panels\MapPanel::create($panel);
    }

  }
}

