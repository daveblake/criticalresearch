<?php
namespace Backend\Auth;

use Backend\BackendBaseController;

class AuthController extends BackendBaseController
{
  protected function _canAccess()
  {
    $this->beforeFilter(
      'auth.admin.guest',
      array('except' => ['getIndex', 'getLogout'])
    );

    $this->beforeFilter(
      'auth.admin',
      array('except' => ['getLogin', 'postLogin'])
    );
  }

  public function getIndex()
  {
    $this->layout->content = \View::make('backend.dashboard')
      ->with('username', ucwords(\Auth::admin()->get()->username));
  }

  public function getLogin()
  {
    $this->layout->content = \View::make('backend.auth.login')
      ->with('email', \Input::old('email'));
  }

  public function postLogin()
  {
    $rules = ['email' => 'required', 'password' => 'required'];

    $validator = \Validator::make(
      \Input::all(),
      $rules
    );
    if($validator->fails())
    {
      return \Redirect::to('/login')->withInput()->withErrors($validator);
    }

    //Validate against email or username
    $field = filter_var(\Input::get('email', ''), FILTER_VALIDATE_EMAIL)
      ? 'email' : 'username';

    $credentials = [
      $field     => \Input::get('email'),
      'password' => \Input::get('password')
    ];

    if(!\Auth::admin()->attempt($credentials))
    {
      return \Redirect::to('/login')->withInput()->withErrors(
        ['password' => 'Your ' . $field . ' or password is incorrect']
      );
    }

    return \Redirect::to('');
  }

  public function getLogout()
  {
    \Auth::admin()->logout();
    return \Redirect::to('/login');
  }
}
