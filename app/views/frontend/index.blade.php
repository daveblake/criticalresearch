@section('content')

  <header class="cf">

    <a id="logo" href="/"><img src="/img/logo<?php if ( $Page->navColor() == 'blue' ): ?>-blue<?php endif; ?>.svg"
                               alt="Critical Research Logo"/></a>

    <a id="mobile-logo" href="/"><img src="/img/logo-small.svg"
                               alt="Critical Research Logo"/></a>


    <a id="mobile-nav" href="#">
      <span></span>
      <span></span>
      <span></span>
    </a>

    <nav class="{{{$Page->navColor()}}}">
      <ul>
        <li <?= $Page->slug == 'what-we-do' ? 'class="active"' : ''; ?>><a href="/what-we-do">What we do</a></li>
        <li <?= $Page->slug == 'our-work' ? 'class="active"' : ''; ?>><a href="/our-work">Our Work</a></li>
        <li <?= $Page->slug == 'team' ? 'class="active"' : ''; ?>><a href="/team">Team</a></li>
        <li <?= $Page->slug == 'contact' ? 'class="active"' : ''; ?>><a href="/#contactus">Contact</a></li>
      </ul>

    </nav>
  </header>

  <div class="panelContainer">

    <?php foreach ( $Page->panels() as $Panel ): ?>
    {{$Panel->render()}}
    <?php endforeach; ?>
    <?php if ( count($Page->panels()) == 0 ): ?>
    <section class="panel">
      <h2>Coming soon</h2>
    </section>
    <?php endif; ?>

    <footer class="cf">
      <div class="wrap">
        <a id="footerLogo" class="left" href="#"><img src="/img/logo-small.svg" alt="Critical Research"/></a>
        <section class="right">
          <p class="copy">&copy; <?= date('Y'); ?> Critical Research</p>
          <nav>
            <ul>
              <li><a href="/disclaimer">Disclaimer</a></li>
              <li><a href="/terms">Terms &amp; Conditions</a></li>
              <li><a href="/company-info">Company Info</a></li>
              <li><a href="/respondent-guarantee">Respondent Guarantee</a></li>
            </ul>
          </nav>
        </section>
      </div>
    </footer>
  </div>
@stop
