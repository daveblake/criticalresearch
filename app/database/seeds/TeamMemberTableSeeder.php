<?php

class TeamMemberTableSeeder extends Seeder
{

  public function run()
  {

    $teamNames = [
        'ben_farr',
        'caroline_robertson',
        'charlotte_caseley',
        'chris_exley',
        'chris_grace',
        'clara_williams',
        'dennis_krushner',
        'derek_farr',
        'emma_wells',
        'geoff_hutton',
        'jack_chapman',
        'james_hopkins',
        'jon_wood',
        'karen_dowley',
        'marc_jacobs',
        'michael_tresise',
        'nick_williams',
        'nigel_marriott',
        'pat_fraser',
        'phil_reilly',
        'rehnuma_chowdhury',
        'ro_marriott',
        'sam_grierson',
        'simon_hume',
        'steve_pick'
    ];
    $teamMembers = [];

    foreach ( $teamNames as $member )
    {
      $featured = $member == 'emma_wells';
      $memberName = ucwords(str_replace('_',' ', $member));
      $teamMembers[] =
        [
          'name'        => $memberName,
          'job_title'   => 'Job Title',
          'quote'       => "This is a quote!\nHere is a quote it's over a
        few lines",
          'description' => 'Accusamus et iusto odio dignissimos ducimus qui
          blanditiis praesentium voluptatum deleniti atque corrupti quos dolores
          et quas molestias excepturi sint occaecati cupiditate non provident,
          similique sunt in culpa qui officia deserunt mollitia animi, id est
          laborum et dolorum fuga. Harum quidem rerum facilis est et expedita
          distinctio nam libero tempore.',
          'image_path'  => $member . '.jpg',
          'featured'    => $featured
        ];
    }

    $usedSlugs = [];
    DB::table('team_members')->truncate();
    foreach($teamMembers as $i => $teamMember)
    {
      $j = 0;
      do
      {
        $teamMember['slug'] = Illuminate\Support\Str::slug($teamMember['name']);
        if($j > 0)
        {
          $teamMember['slug'] .= '-' . $j;
        }
        $j++;
      }
      while(in_array($teamMember['slug'], $usedSlugs));
      $usedSlugs[] = $teamMember['slug'];

      $teamMember['weight'] = $i;
      \CR\Models\TeamMember::create($teamMember);
    }
  }
}

