<?php
namespace Frontend;

use CR\Models\Page;
use CR\Models\Panels\TeamMemberPanel;
use CR\Models\TeamMember;

class TeamController extends \BaseController
{
  /**
   * The layout that should be used for responses.
   */
  protected $layout = 'frontend.layouts.master';

  public function __construct()
  {
    $this->beforeFilter('auth.dev');
  }

  public function getProfile($slug)
  {

    try {
      $TeamMember = TeamMember::where('slug','=',$slug)->firstOrFail();
    } catch ( \Illuminate\Database\Eloquent\ModelNotFoundException $e) {
      \App::abort(404);
    }

    $TeamMemberPanel = (new TeamMemberPanel)->setTeamMember($TeamMember);


    $Page = new Page();
    $Page->panels()->add($TeamMemberPanel);

    $this->layout
      ->with('bodyClass', 'teamMember')
      ->with('Page', $Page);

    $this->layout->content = \View::make('frontend.index')
      ->with('Page', $Page);
  }
}
