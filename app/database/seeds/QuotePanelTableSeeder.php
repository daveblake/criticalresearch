<?php

class QuotePanelTableSeeder extends Seeder
{

  public function run()
  {
    $panels = [
      [
        'uniqueId' => 'quotePanel',
        'quote' => 'Critical did a superb job for us, providing excellent advice throughout the project and really
          going the extra mile to offer us a very professional service. I can highly recommend them.',
        'quote_by' => 'Peter Lockley, Head of Transport Policy, WWF-UK',
        'button_link' => '/our-clients',
        'button_text' => 'Our Clients',
        'image_path' => '/img/panels/teeth.png',
        'image_alt' => 'Clockwork Teeth',
        'class_names' => 'greenBackground',
        'enabled' => 1,
        'global' => 0
      ]
    ];


    DB::table('panels_quotes')->truncate();
    foreach ($panels as $panel) {
      if (! isset($panel['uniqueId']) )
        $panel['uniqueId'] = uniqid();
      \CR\Models\Panels\QuotePanel::create($panel);
    }

  }
}

