<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamMembersTable extends Migration {

	public static $table = 'team_members';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!\Schema::hasTable(static::$table)) {
			\Schema::create(
				static::$table,
				function ($table) {
					$table->increments('id')->unsigned();

					$table->string('name', 200);
					$table->string('slug', 200)->unique();
					$table->string('job_title', 200);
					$table->string('quote', 400);
					$table->string('description', 2000);
					$table->string('image_path', 200);

					$table->boolean('featured');
					$table->integer('weight')->unsigned();

					$table->timestamps();
					$table->softDeletes();

					$table->index('weight');
					$table->index('featured');

				}
			);
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\Schema::dropIfExists(static::$table);
	}

}
