@if (Auth::admin()->check())
  <div class="col-sm-3 col-md-2 sidebar">
    <ul class="nav nav-sidebar">
      @foreach( $navigation as $link )
      <li @if($link['active'])class="active"@endif>
        <a href="{{$link['href']}}">{{{$link['name']}}}
          @if($link['active'])<span class="sr-only">(current)</span>@endif
        </a>
      </li>
      @endforeach
    </ul>
  </div>
@endif