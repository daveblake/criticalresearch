<?php

namespace CR\Models\Panels;

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Illuminate\View\View;

class TeamMembersPanel extends BasePanel
{

  use SoftDeletingTrait;

  protected $_type = 'TeamMembersPanel';


  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'panels_team_members';

  protected $fillable = [
    'uniqueId',
    'class_names',
    'enabled',
    'global'
  ];

  protected $guarded = ['id'];

  protected $dates = ['deleted_at'];

  public function teamMembers()
  {
    $featuredIds = [0];
    foreach ($this->getPage()->panels() as $Panel) {
      if ( $Panel->getType() === 'FeaturedTeamMemberPanel') {
        $featuredIds[] = $Panel->teamMember()->id;
      }
    }
    return \CR\Models\TeamMember::whereNotIn('id', $featuredIds)->get();
  }
}
