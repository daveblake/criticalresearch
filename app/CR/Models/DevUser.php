<?php

namespace CR\Models;

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;

class DevUser extends \Eloquent implements UserInterface
{

  use UserTrait;

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'dev_users';

  protected $fillable = array('email');

  protected $guarded = array('id', 'password');

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = array('password', 'remember_token');

  public function setPasswordAttribute($password)
  {
    $this->attributes['password'] = \Hash::make($password);
  }
}
