<?php namespace CR\Helpers\Seed\Console;

use Symfony\Component\Console\Input\InputOption;

class SeedCommand extends \Illuminate\Database\Console\SeedCommand
{

  /**
   * Get the console command options.
   *
   * @return array
   */
  protected function getOptions()
  {
    $options = parent::getOptions();
    $options[] = array(
      'with-user',
      null,
      InputOption::VALUE_OPTIONAL,
      'Seed User Classes (excluded by default)',
      'DatabaseSeeder'
    );
    return $options;
  }

}
