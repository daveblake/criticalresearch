<?php

namespace CR\Models\Panels;

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Illuminate\View\View;

class FeaturedTeamMemberPanel extends BasePanel
{

  use SoftDeletingTrait;

  protected $_type = 'FeaturedTeamMemberPanel';


  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'panels_featured_team_members';

  protected $fillable = [
    'uniqueId',
    'class_names',
    'enabled',
    'global'
  ];

  protected $guarded = ['id'];

  protected $dates = ['deleted_at'];

  public function render()
  {
    $View = parent::render();
    return $View->with('TeamMember', $this->teamMember());
  }

  public function teamMember()
  {
    return \CR\Models\TeamMember::orderBy('featured', 'desc')->orderByRaw('rand()')->first();
  }
}
