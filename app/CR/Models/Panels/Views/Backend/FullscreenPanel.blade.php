<section id="{{{$Panel->uniqueId}}}" class="panel {{{$Panel->class_names}}}">

  <div class="content">
    <h2>{{{$Panel->title}}}</h2>

    <h3>{{{$Panel->strapline}}}</h3>

    {{$Panel->content}}

    <a href="{{{$Panel->button_link}}}" class="button">{{{$Panel->button_text}}}</a>

    <img class="main" src="{{{$Panel->image_path}}}" alt="{{{$Panel->image_alt}}}"/>
  </div>
</section>