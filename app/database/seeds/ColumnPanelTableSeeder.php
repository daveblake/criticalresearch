<?php

class ColumnPanelTableSeeder extends Seeder
{

  public function run()
  {
    $panels = [
    [
        'uniqueId' => 'clients',
        'column_type' => 'column_4',
        'title' => 'We Work In Harmony With...',
        'button_link' => '',
        'button_text' => '',
        'class_names' => 'whiteBackground',
        'enabled' => 1,
        'global' => 0,
        'columns' => [
            [
                'content' => '',
                'title' => "",
                'image_path' => '/img/clients/hibu.jpg',
                'image_alt' => 'Hibu'
            ],
            [
                'content' => '',
                'title' => "",
                'image_path' => '/img/clients/sunlife.jpg',
                'image_alt' => 'Sunlife'
            ],
            [
                'content' => '',
                'title' => "",
                'image_path' => '/img/clients/axa.jpg',
                'image_alt' => 'AXA'
            ],
            [
                'content' => '',
                'title' => "",
                'image_path' => '/img/clients/rsa.jpg',
                'image_alt' => 'RSA'
            ],
            [
                'content' => '',
                'title' => "",
                'image_path' => '/img/clients/directline.jpg',
                'image_alt' => 'Direct Line'
            ],
            [
                'content' => '',
                'title' => "",
                'image_path' => '/img/clients/ocr.jpg',
                'image_alt' => 'OCR'
            ],
            [
                'content' => '',
                'title' => "",
                'image_path' => '/img/clients/ageUK.jpg',
                'image_alt' => 'Age UK'
            ],
            [
                'content' => '',
                'title' => "",
                'image_path' => '/img/clients/wwf.jpg',
                'image_alt' => 'WWF'
            ],
            [
                'content' => '',
                'title' => "",
                'image_path' => '/img/clients/abrsm.jpg',
                'image_alt' => 'ABRSM'
            ],
            [
                'content' => '',
                'title' => "",
                'image_path' => '/img/clients/mozilla.jpg',
                'image_alt' => 'Mozilla'
            ],
            [
                'content' => '',
                'title' => "",
                'image_path' => '/img/clients/fca.jpg',
                'image_alt' => 'FCA'
            ],
            [
                'content' => '',
                'title' => "",
                'image_path' => '/img/clients/tesco.jpg',
                'image_alt' => 'Tesco'
            ],
            [
                'content' => '',
                'title' => "",
                'image_path' => '/img/clients/cii.jpg',
                'image_alt' => 'CII'
            ],
            [
                'content' => '',
                'title' => "",
                'image_path' => '/img/clients/barclays-corporate.jpg',
                'image_alt' => 'Barclays Corporate'
            ],
            [
                'content' => '',
                'title' => "",
                'image_path' => '/img/clients/the-fa.jpg',
                'image_alt' => 'The FA'
            ],
            [
                'content' => '',
                'title' => "",
                'image_path' => '/img/clients/red-cross.jpg',
                'image_alt' => 'British Red Cross'
            ]
        ]
    ],
      [
        'uniqueId' => 'testimonials',
        'column_type' => 'column_3',
        'title' => 'Don\'t Take Our Word For It...',
        'button_link' => '/contact',
        'button_text' => 'Get In Touch',
        'button_qualifier' => 'To find out how Critical can help you,',
        'class_names' => 'greyBackground',
        'enabled' => 1,
        'global' => 0,
        'columns' => [
          [
            'content' => '“Critical are one of the best market research agencies around. They are great to work with
            and always go the extra mile to fully understand our needs and deliver valuable insight. Their pro-activity,
            intelligence and experience adds tremendous value to all the
            research they conduct for Yell- a truly outstanding service.”',
            'title' => "Jan Barkatullah,\nHead of Market Research, Yell'"
          ],
          [
            'content' => '“I am consistently delighted with Critical\'s ability to pull rabbits out of hats for us and
            they approach everything with creativity, intelligence, professionalism and good humour.
            If anyone can make it happen, it\'s them.”
',
            'title' => "Fay Purves, Managing Director,\nWhat's Next Business Consultancy Ltd"
          ],
          [
            'content' => '"It\'s a pleasure to work with an agency who clearly understand the audience and the survey
            requirements. They add value throughout the research process and on top of that they provide excellent
            customer service, delivering on time and on budget"',
            'title' => "Sabine Rumscheidt,\nEconomic Analysis Manager, South East England Development Agency"
          ]
        ]
      ],
      [
        'uniqueId' => 'certifications',
        'column_type' => 'column_4',
        'title' => 'Recognition? Yes. We’re Known For Going Further',
        'button_link' => '',
        'button_text' => '',
        'class_names' => 'greyBackground',
        'enabled' => 1,
        'global' => 0,
        'columns' => [
          [
            'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,
            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            Ut enim ad minim veniam, quis nostrud exerci tation ullamco laboris.',
            'title' => "",
            'image_path' => '/img/logos/iso9001.jpg',
            'image_alt' => 'ISO 9001 Certified'
          ],
          [
            'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,
            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            Ut enim ad minim veniam, quis nostrud exerci tation ullamco laboris.',
            'title' => "",
            'image_path' => '/img/logos/esomar.jpg',
            'image_alt' => 'Esomar'
          ],
          [
            'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,
            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            Ut enim ad minim veniam, quis nostrud exerci tation ullamco laboris.',
            'title' => "",
            'image_path' => '/img/logos/big.jpg',
            'image_alt' => 'BIG Logo'
          ],
          [
            'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,
            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            Ut enim ad minim veniam, quis nostrud exerci tation ullamco laboris.',
            'title' => "",
            'image_path' => '/img/logos/mrs.jpg',
            'image_alt' => 'MRS Evidence Matters'
          ]
        ]
      ]
    ];


    DB::table('panels_columns')->truncate();
    DB::table('panels_columns_content')->truncate();
    foreach ($panels as $panel) {
      $columns = [];
      if (isset($panel['columns'])) {
        $columns = $panel['columns'];
        unset($panel['columns']);
      }
      $ColumnPanel = \CR\Models\Panels\ColumnPanel::create($panel);
      foreach ($columns as $i => $column) {
        $column['weight'] = $i;
        $columns[$i] = new \CR\Models\Panels\ColumnPanelContent($column);
      }
      $ColumnPanel->columns()->saveMany($columns);
    }

  }
}

