<?php

namespace CR\Helpers;

class File
{

  /**
   * @param $path
   *
   * @return mixed
   */
  static function unique($path)
  {

    if(!file_exists($path))
    {
      return $path;
    }

    do
    {
      $aPathInfo = pathinfo($path);

      $end = strrchr($aPathInfo['filename'], '[');

      if(strlen($end) == 0)
      {
        $aPathInfo['filename'] .= '[1]';
      }
      elseif(substr($end, strlen($end) - 1, 1) != ']')
      {
        $aPathInfo['filename'] .= '[1]';
      }
      elseif(!is_numeric(substr($end, 1, strlen($end) - 2)))
      {
        $aPathInfo['filename'] .= '[1]';
      }
      elseif((string)substr($end, 1, strlen($end) - 2) !== (string)(int)(substr(
          $end,
          1,
          strlen($end) - 2
        ))
      )
      {
        $aPathInfo['filename'] .= '[1]';
      }
      else
      {
        $index = (int)(substr($end, 1, strlen($end) - 2));
        $index++;

        $endlength = strlen($end);
        $aPathInfo['filename'] = substr(
          $aPathInfo['filename'],
          0,
          strlen($aPathInfo['filename']) - $endlength
        );
        $aPathInfo['filename'] .= '[' . $index . ']';
      }

      $path = $aPathInfo['dirname'] . '/' . $aPathInfo['filename'] . '.' . $aPathInfo['extension'];
    }
    while(file_exists($path));

    return $path;
  }
}
