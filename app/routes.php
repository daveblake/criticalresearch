<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

$domain = \CR\Helpers\UrlHelper::getHost();

/*
 * Holding Page Routes
 */
Route::group(
  array('domain' => 'www.' . $domain),
  function ()
  {
    Route::controller('/', 'Holding\HoldingController');
    Route::any(
      '{all}',
      function ()
      {
        Redirect::to('/', 301);
      }
    )->where('all', '.*');
  }
);

/*
 * Main Frontend Website Routes
 */
Route::group(
  array('domain' => 'ww.' . $domain),
  function ()
  {
    Route::get(
      'team/{slug}',
      array('uses' => 'Frontend\TeamController@getProfile')
    );
    Route::any('{slug}', array('uses' => 'Frontend\PageController@getPage'))
      ->where('slug', '.*');
  }
);

/*
 * Admin Routes
 */
Route::group(
  array('domain' => 'admin.' . $domain),
  function ()
  {
    Route::controller('/team', 'Backend\Team\TeamController');
    Route::controller('/page', 'Backend\Page\PageController');
    Route::controller('/navigation', 'Backend\Navigation\NavigationController');
    Route::controller('/', 'Backend\Auth\AuthController');
  }
);

/*
 * Redirect to www. if we've arrive on just the domain...
 */
Route::group(
  array('domain' => $domain),
  function () use ($domain)
  {
    Route::any(
      '{all}',
      function () use ($domain)
      {
        return Redirect::to('http://www.' . $domain, 301);
      }
    )->where('all', '.*');
  }
);

App::missing(
  function ($exception)
  {
    return '404';
    //return Response::view('errors.missing', array(), 404);
  }
);