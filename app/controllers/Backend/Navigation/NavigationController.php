<?php
namespace Backend\Navigation;

use Backend\BackendBaseController;

class NavigationController extends BackendBaseController
{
  public function getIndex()
  {
    $this->layout->content = \View::make('backend.navigation.list');
  }

  public function getCreate()
  {
    $this->layout->content = \View::make('backend.navigation.list');
  }

  public function postCreate() {

  }

  protected function _setActive()
  {
    $this->_navigation['navigation']['active'] = true;
  }
}
