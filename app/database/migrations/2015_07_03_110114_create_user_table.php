<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{

    public static $table = 'users';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!\Schema::hasTable(static::$table))
        {
            \Schema::create(
                static::$table,
                function ($table)
                {
                    $table->increments('id');
                    $table->string('email', 100)->unique();
                    $table->string('username', 100)->unique();
                    $table->string('password', 100);
                    $table->string('remember_token', 100)->nullable();
                    $table->timestamps();
                }
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(\Schema::hasTable(static::$table))
        {
            \Schema::drop(static::$table);
        }
    }
}
