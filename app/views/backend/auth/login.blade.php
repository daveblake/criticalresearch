@extends('backend.layouts.master')

@section('content')

  <div class="row">
    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
      <div class="login-panel panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Sign In</h3>
        </div>
        <div class="panel-body">

          @if ($errors->count())
            <div class="alert  alert-danger">
              <ul>
                {{ implode('', $errors->all('<li>:message</li>')) }}
              </ul>
            </div>
          @endif

          {{ Form::open(array('url' => 'login')) }}
          <fieldset>
            <div class="form-group">
              {{ Form::text('email', Input::old('email'), array('class' => 'form-control', 'placeholder' => 'E-mail or Username')) }}
            </div>
            <div class="form-group">
              {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password')) }}
            </div>
            <div class="checkbox">
              <label>
                <input name="remember" type="checkbox"
                       @if( Input::old('remember',1) ) checked @endif value="1">Remember
                                                                                Me
              </label>
            </div>
            {{ Form::submit('Login', ['class'=> 'btn btn-sm btn-success'] ) }}
          </fieldset>
          {{ Form::close() }}
        </div>
      </div>
    </div>
    <!-- col-md-4 -->
  </div><!-- row -->

@endsection