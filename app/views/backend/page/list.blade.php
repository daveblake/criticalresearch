@extends('backend.layouts.master')

@section('content')

<h3>Manage Pages</h3>

@if(Session::has('success'))
<div class="alert  alert-success">
    {{ Session::get('success') }}
</div>
@endif

@if ($errors->count())
<div class="alert  alert-danger">
    <ul>
        {{ implode('', $errors->all('<li>:message</li>')) }}
    </ul>
</div>
@endif

<p><a class="btn btn-primary" href="/page/create">Add a new page</a></p>





@if(count($pages) == 0)
<div class="alert">
    No pages have been created yet.<br />
    <a href="create">Create Page</a>
</div>
@else
<table class="table table-striped">
    <thead>
    <tr>
        <th>Enabled</th><th>Title</th><th>Slug</th><th>&nbsp;</th>
    </tr>
    </thead>
    @foreach($pages as $page)
    <tr>

        <td>
            <input <?= $page['enabled'] ? 'checked' : ''; ?>
                type="checkbox"
                name="enabled[]"
                id="enabled_<?= $page['id']; ?>" />
        </td>
        <td>
            <label for="enabled_<?=$page['id']; ?>">{{{$page['title']}}}</label></td>
        <td>/{{{$page['slug']}}}</td>
        <td>
            <a class="btn btn-warning" href="page/edit/<?= $page['id']; ?>">Edit</a>
            <a class="btn btn-danger" href="page/delete/<?= $page['id']; ?>">Delete</a>
        </td>
    </tr>
    @endforeach
</table>
@endif

@endsection