<?php
namespace Backend\Team;

use Backend\BackendBaseController;
use CR\Models\TeamMember;

class TeamController extends BackendBaseController
{
  public function getIndex()
  {
    $teamMembers           = TeamMember::all()->toArray();
    $this->layout->content = \View::make('backend.team.list')
      ->with('teamMembers', $teamMembers);
  }

  public function getCreate()
  {
    $this->_form();
  }

  public function getEdit($id)
  {

    $teamMember = (new TeamMember)->find($id);
    try
    {
      if(is_null($teamMember))
      {
        throw new \Exception('Invalid TeamMember id');
      }
      $this->_form($teamMember);
    }
    catch(\Exception $e)
    {
      return \Redirect::to('team')
        ->withErrors(
          [
            'Team member could not be deleted',
            $e->getMessage()
          ]
        );
    }
  }

  public function _form($teamMember = null)
  {

    $formType = is_null($teamMember) ? 'Add' : 'Edit';
    if(is_null($teamMember))
    {
      $teamMember = new TeamMember;
    }

    $this->layout->content = \View::make('backend.team.form')
      ->with('teamMember', $teamMember)
      ->with('formType', $formType);
  }

  public function postCreate()
  {
    return $this->_save((new TeamMember));
  }

  public function postEdit($id)
  {

    $teamMember = (new TeamMember)->find($id);
    if(is_null($teamMember))
    {
      throw new \Exception('Invalid TeamMember id');
    }

    return $this->_save($teamMember);
  }

  protected function _save($teamMember)
  {

    $path = 'team' . (is_null($teamMember->id)
        ? '/create' : '/edit/' . $teamMember->id);

    $rules = [
      'name'  => 'required',
      'slug'  => 'required|alpha_dash|unique:team_members,slug,' . $teamMember->id . ',id',
      'image' => [],
      'thumb' => []
    ];

    $rules['image'][] = ($teamMember->image_name == '') ? 'required' : '';
    $rules['image'][] = (\Input::hasFile('image')) ? 'image' : '';
    $rules['image']   = implode('|', array_filter($rules['image']));

    $rules['thumb'][] = ($teamMember->thumb_name == '') ? 'required' : '';
    $rules['thumb'][] = (\Input::hasFile('thumb')) ? 'image' : '';
    $rules['thumb']   = implode('|', array_filter($rules['thumb']));

    $validator = \Validator::make(\Input::all(), $rules);
    if($validator->fails())
    {
      return \Redirect::to($path)->withErrors($validator)->withInput();
    }

    $imagePath   = public_path() . '/img/team/';
    $thumbPath   = $imagePath . 'thumbs/';
    $deleteFiles = [];

    if(\Input::hasFile('thumb'))
    {
      $destinationPath = $thumbPath . \Input::file('thumb')
          ->getClientOriginalName();

      $destinationPath = \CR\Helpers\File::unique($destinationPath);
      \Input::file('thumb')->move(
        dirname($destinationPath),
        basename($destinationPath)
      );

      if($teamMember->thumb_name)
      {
        $deleteFiles[] = $thumbPath . $teamMember->thumb_name;
      }
      $teamMember->thumb_name = basename($destinationPath);
    }

    if(\Input::hasFile('image'))
    {
      $destinationPath = $imagePath . \Input::file('image')
          ->getClientOriginalName();

      $destinationPath = \CR\Helpers\File::unique($destinationPath);
      \Input::file('image')->move(
        dirname($destinationPath),
        basename($destinationPath)
      );

      if($teamMember->image_name)
      {
        $deleteFiles[] = $imagePath . $teamMember->image_name;
      }
      $teamMember->image_name = basename($destinationPath);
    }

    $teamMember->name        = \Input::get('name');
    $teamMember->slug        = \Input::get('slug');
    $teamMember->job_title   = \Input::get('job_title', '');
    $teamMember->quote       = \Input::get('quote', '');
    $teamMember->description = \Input::get('description', '');
    $teamMember->featured    = (bool)\Input::get('featured', false);
    $teamMember->save();

    foreach($deleteFiles as $filePath)
    {
      \File::delete($filePath);
    }

    return \Redirect::to($path)->withSuccess('Team Member Saved');
  }

  public function getDelete($id)
  {
    try
    {
      $teamMember = (new TeamMember)->find($id);
      if(is_null($teamMember))
      {
        throw new \Exception('Invalid TeamMember id');
      }
      if($teamMember->thumb_name)
      {
        \File::delete(
          public_path() . '/img/team/thumbs/' . $teamMember->thumb_name
        );
      }
      if($teamMember->image_name)
      {
        \File::delete(
          public_path() . '/img/team/' . $teamMember->image_name
        );
      }

      $teamMember->delete();

      return \Redirect::to('team')->withSuccess(
        $teamMember['name'] . ' Removed'
      );
    }
    catch(\Exception $e)
    {
      return \Redirect::to('team')
        ->withErrors(
          [
            'Team member could not be deleted',
            $e->getMessage()
          ]
        );
    }
  }


  protected function _setActive()
  {
    $this->_navigation['team']['active'] = true;
  }
}
