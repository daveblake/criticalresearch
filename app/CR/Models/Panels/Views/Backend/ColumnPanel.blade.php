<section id="{{{$Panel->uniqueId}}}" class="panel panelColumns {{{$Panel->class_names}}}">

  <div class="content">
    <h2>{{{$Panel->title}}}</h2>


    <?php if ( count($Panel->columns) != 0 ): ?>
    <ul class="cf columnList {{{$Panel->column_type}}}">
      <?php foreach ( $Panel->columns as $Column ): ?>
      <li>

        <?php if ( $Column->image_path ): ?>
        <div class="imageWrap">
          <img src="{{{$Column->image_path}}}" alt="{{{$Column->image_alt}}}}" />
        </div>
        <?php endif; ?>

        {{{$Column->content}}}
        <?php if ( $Column->title ): ?>
          <strong>{{nl2br(e($Column->title))}}</strong>
        <?php endif; ?>
      </li>
      <?php endforeach; ?>
    </ul>
    <?php endif; ?>

    <?php if ( $Panel->button_text ): ?>
    <div class="buttonWrap">
    {{{$Panel->button_qualifier}}} <a href="{{{$Panel->button_link}}}" class="button">{{{$Panel->button_text}}}</a>
    </div>
    <?php endif; ?>

  </div>
</section>