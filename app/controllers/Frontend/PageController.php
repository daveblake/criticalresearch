<?php
namespace Frontend;

use CR\Models\Page;

class PageController extends \BaseController
{
  /**
   * The layout that should be used for responses.
   */
  protected $layout = 'frontend.layouts.master';

  public function __construct()
  {
    $this->beforeFilter('auth.dev');
  }

  public function getPage($slug)
  {
    $slugArray = $this->slugToArray($slug);
    $slugReal = $this->arrayToSlug($slugArray);

    // This just makes sure that paths with two or more slashes
    // are redirected to the corrected path (SEO, duplicate content etc...)
    // //path//to//my//page -> /path/to/my/page
    if ($slugReal !== $slug) {
      return \Redirect::to($slugReal, 301);
    }

    try {
      $Page = (new Page)->fromSlugArray($slugArray);
    } catch ( \Illuminate\Database\Eloquent\ModelNotFoundException $e) {
      \App::abort(404);
    }
    $this->layout
      ->with('bodyClass', $Page->slug)
      ->with('Page', $Page);

    $this->layout->content = \View::make('frontend.index')
      ->with('Page', $Page);
  }

  private function slugToArray($slug)
  {
    return array_filter(explode('/', $slug));
  }

  private function arrayToSlug($slugArray)
  {
    if (empty($slugArray)) {
      $slugArray = ['', ''];
    }
    return implode('/', $slugArray);
  }
}
