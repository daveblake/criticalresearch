<?php

namespace Backend;

class BackendBaseController extends \BaseController
{
  /**
   * The layout that should be used for responses.
   */
  protected $layout = 'backend.layouts.master';

  protected $_navigation = [
    'dashboard' => [
      'href'   => '/',
      'name'   => 'Dashboard',
      'active' => false,
    ],
    'page'      => [
      'href'   => '/page',
      'name'   => 'Manage Pages',
      'active' => false,
    ],
    'team'      => [
      'href'   => '/team',
      'name'   => 'Manage Team',
      'active' => false,
    ],
    'navigation' => [
      'href'   => '/navigation',
      'name'   => 'Manage Navigation',
      'active' => false,
      ]
  ];

  public function __construct()
  {

    $this->_canAccess();

    $this->_setActive();

    \View::share('navigation', $this->_navigation);
  }

  protected function _canAccess()
  {
    $this->beforeFilter('auth.admin');
  }

  protected function _setActive()
  {
    $this->_navigation['dashboard']['active'] = true;
  }
}
