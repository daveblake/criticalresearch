@extends('backend.layouts.master')

@section('content')

  <h3><?= $formType; ?> Team Member</h3>

  @if(Session::has('success'))
    <div class="alert  alert-success">
      {{ Session::get('success') }}
    </div>
  @endif

  @if ($errors->count())
    <div class="alert  alert-danger">
      <ul>
        {{ implode('', $errors->all('<li>:message</li>')) }}
      </ul>
    </div>
  @endif

  <p><a class="btn btn-primary" href="/team">Manage team members</a></p>

  {{ Form::open(array('files'=> true)) }}
  <fieldset>
    <div class="form-group">
      <label for="name">Name</label>
      {{ Form::text('name', Input::old('name', $teamMember->name), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
      <label for="slug">Slug</label>
      {{ Form::text('slug', Input::old('slug', $teamMember->slug), array('class' => 'form-control')) }}
      <p class="help-block">The slug is used for the team member url E.g.<em>/team/john-smith</em>
      </p>
    </div>

    <div class="form-group">
      <label for="job_title">Job Title</label>
      {{ Form::text('job_title', Input::old('job_title', $teamMember->job_title), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
      <label for="description">Description</label>
      {{ Form::textarea('description', Input::old('description', $teamMember->description), array('class' => 'form-control')) }}
    </div>


    <div class="form-group">
      <label for="quote">Quote</label>
      {{ Form::textarea('quote', Input::old('quote', $teamMember->quote), array('class' => 'form-control', 'rows'=>3)) }}
    </div>


    <div class="form-group">
      <label for="image">Main Image</label>
      <input name="image" type="file" id="image">
      @if ( $teamMember->image_name )
        <img width="200" class="img-thumbnail"
             src="/img/team/<?= rawurlencode($teamMember->image_name); ?>"/>
      @endif
    </div>


    <div class="form-group">
      <label for="thumb">Thumbnail</label>
      <input name="thumb" type="file" id="thumb">

      @if ( $teamMember->thumb_name )
        <img width="200" class="img-thumbnail"
             src="/img/team/thumbs/<?= rawurlencode(
                 $teamMember->thumb_name
             ); ?>"/>
      @endif
    </div>

    <div class="form-group">
      <label for="featured">Featured</label>
      {{Form::checkbox('featured', '1', \Input::old('featured', $teamMember->featured));}}
    </div>


    {{ Form::submit('Save', ['class'=> 'btn btn-sm btn-success'] ) }}
  </fieldset>
  {{ Form::close() }}


@endsection