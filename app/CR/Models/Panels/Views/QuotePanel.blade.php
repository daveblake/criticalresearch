<section id="{{{$Panel->uniqueId}}}" class="cf panel {{{$Panel->class_names}}}">

  <div class="content">

    <blockquote>
      {{{$Panel->quote}}}
    </blockquote>
    <p>
      {{{$Panel->quote_by}}}
    </p>

    <a href="{{{$Panel->button_link}}}" class="button">{{{$Panel->button_text}}}</a>


    <img class="main" src="{{{$Panel->image_path}}}" alt="{{{$Panel->image_alt}}}"/>
  </div>
</section>