<?php

namespace CR\Models\Panels;

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Illuminate\View\View;

class ColumnPanelContent extends \Eloquent
{

  use SoftDeletingTrait;


  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'panels_columns_content';

  protected $fillable = [
    'panels_columns_id',
    'title',
    'content',
    'image_path',
    'image_alt',
    'weight'
  ];

  protected $guarded = ['id'];

  protected $dates = ['deleted_at'];

  public function panel()
  {
    return $this->belongsTo(' CR\Models\Panels\ColumnPanel', 'panels_columns_id');
  }

}
