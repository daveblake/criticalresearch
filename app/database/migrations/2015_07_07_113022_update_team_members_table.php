<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTeamMembersTable extends Migration
{

  public static $table = 'team_members';

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    \Schema::table(
      static::$table,
      function ($table)
      {
        $table->renameColumn('image_path', 'image_name');
        $table->string('thumb_name', 200)->after('image_path');
      }
    );

    \DB::statement('UPDATE team_members SET thumb_name = image_name');
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    \Schema::table(
      static::$table,
      function ($table)
      {
        $table->renameColumn('image_name', 'image_path');
        $table->dropColumn('thumb_name');
      }
    );
  }

}
