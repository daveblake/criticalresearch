<?php
namespace Holding;

class HoldingController extends \BaseController
{

  /**
   * The layout that should be used for responses.
   */
  protected $layout = 'holding.layouts.master';

  /**
   * Instantiate a new HoldingController instance.
   */
  public function __construct()
  {
    $this->beforeFilter(
      'auth.dev.loggedIn',
      array('except' => ['getIndex', 'postIndex', 'getLogout'])
    );
  }

  public function getIndex()
  {
    $this->layout->content = \View::make('holding.index');
  }

  public function getLogin()
  {
    $this->layout->content = \View::make('holding.login')
      ->with('email', \Input::old('email'));
  }

  public function postLogin()
  {
    $rules = ['email' => 'required|email', 'password' => 'required'];

    $validator = \Validator::make(
      \Input::all(),
      $rules
    );
    if ($validator->fails()) {
      return \Redirect::to('/login')->withInput()->withErrors($validator);
    }

    $credentials = [
      'email' => \Input::get('email'),
      'password' => \Input::get('password')
    ];

    if (!\Auth::dev()->attempt($credentials)) {
      return \Redirect::to('/login')->withInput()->withErrors(
        ['password' => 'Your email or password is incorrect']
      );
    }

    return \Redirect::to(\CR\Helpers\UrlHelper::buildUrl('ww'));
  }

  public function getLogout()
  {
    \Auth::dev()->logout();
    return \Redirect::to('/login');
  }
}
