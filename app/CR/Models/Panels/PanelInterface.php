<?php

namespace CR\Models\Panels;


interface PanelInterface
{

  public function getType();

  public function render();
}