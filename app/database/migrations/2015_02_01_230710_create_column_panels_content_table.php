<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColumnPanelsContentTable extends Migration {

	public static $table = 'panels_columns_content';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!\Schema::hasTable(static::$table)) {
			\Schema::create(
				static::$table,
				function ($table) {
					$table->increments('id')->unsigned();
					$table->integer('panels_columns_id')->unsigned();


					$table->string('title', 200);
					$table->string('content', 400);
					$table->string('image_path', 200);
					$table->string('image_alt', 200);

					$table->integer('weight')->unsigned();


					$table->timestamps();
					$table->softDeletes();

					$table->index('panels_columns_id');
					$table->index('weight');

				}
			);
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\Schema::dropIfExists(static::$table);
	}

}
