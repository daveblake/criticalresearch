<?php

use \CR\Models\DevUser;

class DevUserTableSeeder extends Seeder
{

  public function run()
  {
    DB::table('dev_users')->delete();
    DevUser::create(
      ['email' => 'info@criticalresearch.com', 'password' => 'cr2015']
    );
    DevUser::create(
      ['email' => 'me@daveblake.co.uk', 'password' => 'nimzoted']
    );
  }
}

