<?php

class PagePanelsTableSeeder extends Seeder
{

  public function run()
  {
    $pagePanels = [
      [
        'page_id' => 1,
        'panel_id' => 1,
        'panel_type' => 'FullscreenPanel',
        'weight' => 0
      ],
      [
        'page_id' => 1,
        'panel_id' => 2,
        'panel_type' => 'FullscreenPanel',
        'weight' => 1
      ],
      [
        'page_id' => 1,
        'panel_id' => 3,
        'panel_type' => 'FullscreenPanel',
        'weight' => 2
      ],
      [
        'page_id' => 1,
        'panel_id' => 1,
        'panel_type' => 'QuotePanel',
        'weight' => 3
      ],
      [
        'page_id' => 1,
        'panel_id' => 1,
        'panel_type' => 'MapPanel',
        'weight' => 4
      ],
      [
        'page_id' => 2,
        'panel_id' => 4,
        'panel_type' => 'FullscreenPanel',
        'weight' => 0
      ],
      [
        'page_id' => 2,
        'panel_id' => 5,
        'panel_type' => 'FullscreenPanel',
        'weight' => 1
      ],
      [
        'page_id' => 2,
        'panel_id' => 6,
        'panel_type' => 'FullscreenPanel',
        'weight' => 2
      ],
      [
        'page_id' => 2,
        'panel_id' => 7,
        'panel_type' => 'FullscreenPanel',
        'weight' => 3
      ],
      [
        'page_id' => 2,
        'panel_id' => 8,
        'panel_type' => 'FullscreenPanel',
        'weight' => 4
      ],
      [
        'page_id' => 3,
        'panel_id' => 9,
        'panel_type' => 'FullscreenPanel',
        'weight' => 0
      ],
      [
        'page_id' => 3,
        'panel_id' => 1,
        'panel_type' => 'ColumnPanel',
        'weight' => 1
      ],
      [
        'page_id' => 3,
        'panel_id' => 2,
        'panel_type' => 'ColumnPanel',
        'weight' => 3
      ],
      [
        'page_id' => 3,
        'panel_id' => 3,
        'panel_type' => 'ColumnPanel',
        'weight' => 4
      ],
      [
        'page_id' => 4,
        'panel_id' => 1,
        'panel_type' => 'FeaturedTeamMemberPanel',
        'weight' => 0
      ],
      [
        'page_id' => 4,
        'panel_id' => 1,
        'panel_type' => 'TeamMembersPanel',
        'weight' => 1
      ]
    ];

    DB::table('page_panels')->truncate();
    foreach ($pagePanels as $pagePanel) {
      \CR\Models\Panels\PagePanel::create($pagePanel);
    }

  }
}

