<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{

  public static $table = 'pages';

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    if (!\Schema::hasTable(static::$table)) {
      \Schema::create(
        static::$table,
        function ($table) {
          $table->increments('id')->unsigned();

          $table->integer('parent_id')->unsigned();
          $table->string('slug', 100);
          $table->string('title', 200);
          $table->boolean('enabled')->default(1);
          $table->integer('weight');
          $table->enum('seo', ['automatic', 'custom'])->default('automatic');
          $table->string('meta_title', 250);
          $table->string('meta_keywords', 500);
          $table->string('meta_description', 500);

          $table->timestamps();
          $table->softDeletes();

          $table->unique(['parent_id', 'slug']);
          $table->index('enabled');
          $table->index('weight');

        }
      );
    }
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    \Schema::dropIfExists(static::$table);
  }

}
