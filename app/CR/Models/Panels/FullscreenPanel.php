<?php

namespace CR\Models\Panels;

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Illuminate\View\View;

class FullscreenPanel extends BasePanel
{

  use SoftDeletingTrait;

  protected $_type = 'FullscreenPanel';


  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'panels_fullscreen';

  protected $fillable = [
    'uniqueId',
    'title',
    'strapline',
    'content',
    'button_link',
    'button_text',
    'image_path',
    'image_alt',
    'class_names',
    'enabled',
    'global'
  ];

  protected $guarded = ['id'];

  protected $dates = ['deleted_at'];

}
