<?php
namespace CR\Helpers;

class UrlHelper
{

  protected static $_subDomains = ['www', 'ww', 'admin'];

  private static $_scheme = null;
  private static $_subDomain = null;
  private static $_host = null;
  private static $_port = null;

  static function setConsoleDefaults()
  {
    self::$_scheme = '';
    self::$_subDomain = '';
    self::$_host = 'localhost';
    self::$_port = '';
  }

  static function parseUrl()
  {
    if (\App::runningInConsole()) {
      self::setConsoleDefaults();
      return;
    }

    $parsedURL = parse_url(\Request::fullUrl());

    self::$_scheme = $parsedURL['scheme'];
    self::$_port = isset($parsedURL['port']) ? $parsedURL['port'] : '';
    self::$_host = self::removeSubdomain(
      $parsedURL['host'],
      self::$_subDomain,
      self::$_subDomains
    );
  }

  static function removeSubDomain($domain, &$subDomain, $subDomains = [])
  {
    foreach ($subDomains as $subDomain) {
      if (substr($domain, 0, strlen($subDomain)) == $subDomain) {
        return substr($domain, strlen($subDomain) + 1);
      }
    }
    return $domain;
  }

  static function getScheme()
  {
    if (is_null(self::$_scheme)) {
      self::parseURL();
    }

    return self::$_scheme;
  }

  static function getSubDomain()
  {
    if (is_null(self::$_subDomain)) {
      self::parseURL();
    }

    return self::$_subDomain;
  }

  /**
   * Returns the application host without any of
   * the used subDomains
   *
   * @return string
   */
  static function getHost()
  {
    if (is_null(self::$_host)) {
      self::parseURL();
    }

    return self::$_host;
  }

  static function getPort()
  {
    if (is_null(self::$_port)) {
      self::parseUrl();
    }

    return self::$_port;
  }

  static function buildUrl($subDomain = 'www')
  {
    if (is_null(self::$_host)) {
      self::parseUrl();
    }

    if ($subDomain != '') {
      $subDomain = $subDomain . '.';
    }

    $port = self::getPort();
    if ($port != '') {
      $port = ':' . $port;
    }

    return self::getScheme() . '://' .
    $subDomain . self::getHost() . $port;
  }
}