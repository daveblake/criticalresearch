<?php

use \CR\Models\Page;

class PageTableSeeder extends Seeder
{

  public function run()
  {
    $pages = [
      [
        'parent_id' => 0,
        'slug' => 'index',
        'title' => 'Home Page',
        'enabled' => true,
        'weight' => -1,
        'seo' => 'automatic',
      ],
      [
        'parent_id' => 0,
        'slug' => 'what-we-do',
        'title' => 'What We Do',
        'enabled' => true,
        'weight' => 1,
        'seo' => 'automatic',
      ],
      [
        'parent_id' => 0,
        'slug' => 'our-work',
        'title' => 'Our Work',
        'enabled' => true,
        'weight' => 1,
        'seo' => 'automatic',
      ],
      [
        'parent_id' => 0,
        'slug' => 'team',
        'title' => 'Team',
        'enabled' => true,
        'weight' => 2,
        'seo' => 'automatic',
      ],
      [
        'parent_id' => 0,
        'slug' => 'contact',
        'title' => 'Contact',
        'enabled' => true,
        'weight' => 3,
        'seo' => 'automatic',
      ],
      [
        'parent_id' => 0,
        'slug' => 'disclaimer',
        'title' => 'Disclaimer',
        'enabled' => true,
        'weight' => 3,
        'seo' => 'automatic',
      ]
    ];


    DB::table('pages')->truncate();
    foreach ($pages as $page) {
      Page::create($page);
    }

  }
}

