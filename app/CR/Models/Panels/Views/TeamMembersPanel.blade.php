<section id="{{{$Panel->uniqueId}}}"
         class="panel teamMembers {{{$Panel->class_names}}}">

  <div class="content">
    <?php if ( count($Panel->teamMembers()) != 0 ): ?>
    <ul class="cf teamMembers">
      <?php foreach ( $Panel->teamMembers() as $TeamMember ): ?>
      <li>

        <?php if ( $TeamMember->thumb_name ): ?>
        <div class="imageWrap">
          <img src="/img/team/thumbs/{{{$TeamMember->thumb_name}}}" alt="{{{$TeamMember->name}}}}" />
        </div>
        <?php endif; ?>
        <strong>{{{$TeamMember->name}}}</strong>
        {{{$TeamMember->job_title}}}
        <a href="./team/{{{$TeamMember->slug}}}">Meet {{{$TeamMember->firstName()}}}</a>
      </li>
      <?php endforeach; ?>
    </ul>
    <?php endif; ?>
  </div>
</section>