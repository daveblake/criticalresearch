<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagePanelsTable extends Migration {

	public static $table = 'page_panels';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!\Schema::hasTable(static::$table)) {
			\Schema::create(
				static::$table,
				function ($table) {
					$table->increments('id')->unsigned();
					$table->integer('page_id')->unsigned();
					$table->integer('panel_id')->unsigned();
					$table->string('panel_type',30);
					$table->integer('weight')->unsigned();

					$table->timestamps();
					$table->softDeletes();

					$table->index('page_id');
					$table->index('panel_id');
					$table->index('weight');

				}
			);
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\Schema::dropIfExists(static::$table);
	}

}
