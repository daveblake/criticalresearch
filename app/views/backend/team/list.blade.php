@extends('backend.layouts.master')

@section('content')

<h3>Manage Team Members</h3>

@if(Session::has('success'))
<div class="alert  alert-success">
    {{ Session::get('success') }}
</div>
@endif

@if ($errors->count())
<div class="alert  alert-danger">
    <ul>
        {{ implode('', $errors->all('<li>:message</li>')) }}
    </ul>
</div>
@endif

<p><a class="btn btn-primary" href="/team/create">Add a new team member</a></p>





@if(count($teamMembers) == 0)
<div class="alert">
    No team members have been created yet.<br />
<a href="create">Create Team Member</a>
</div>
@else
<table class="table table-striped">
    <thead>
    <tr>
        <th>Featured</th><th>Name</th><th>Job Title</th><th>&nbsp;</th>
    </tr>
    </thead>
    @foreach($teamMembers as $teamMember)
        <tr>
            <td>
                <input <?= $teamMember['featured'] ? 'checked' : ''; ?>
                    type="checkbox"
                    name="featured[]"
                    id="featured_<?= $teamMember['id']; ?>" />
            </td>
            <td>
                <label for="featured_<?=$teamMember['id']; ?>">{{{$teamMember['name']}}}</label></td>
            <td>{{{$teamMember['job_title']}}}</td>
            <td>
                <a class="btn btn-warning" href="team/edit/<?= $teamMember['id']; ?>">Edit</a>
                <a class="btn btn-danger" href="team/delete/<?= $teamMember['id']; ?>">Delete</a>
            </td>
        </tr>
    @endforeach
</table>
@endif

@endsection