<?php

namespace CR\Models\Panels;

use CR\Models\Page;

class BasePanel extends \Eloquent implements PanelInterface
{

  protected $_page = null;
  protected $_type = 'BasePanel';

  /**
   * @return string
   */
  public function getType()
  {
    return $this->_type;
  }

  /**
   * @param Page $Page
   *
   * @return $this
   */
  public function setPage(Page $Page)
  {
    $this->_page = $Page;
    return $this;
  }

  /**
   * @return $this
   */
  public function getPage()
  {
    return $this->_page;
  }

  public function getPageIds()
  {
    $pageIds    = [];
    $pagePanels = \DB::table('page_panels')
      ->select('page_id')
      ->where(['panel_type' => $this->_type, 'panel_id' => $this->id])
      ->get();
    foreach($pagePanels as $panel)
    {
      $pageIds[] = $panel->page_id;
    }
    return array_unique($pageIds);
  }

  /**
   * @return string
   */
  public function render()
  {

    return \View::make('panel::' . $this->getType())
      ->with('Panel', $this);
  }

  public function renderAdmin()
  {
	
    return \View::make('panel::Backend.' . $this->getType())
      ->with('Panel', $this);
  }
}
