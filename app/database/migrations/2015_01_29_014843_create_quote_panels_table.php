<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotePanelsTable extends Migration {

	public static $table = 'panels_quotes';
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!\Schema::hasTable(static::$table)) {
			\Schema::create(
				static::$table,
				function ($table) {
					$table->increments('id')->unsigned();
					$table->string('uniqueId', 200)->unique();
					$table->string('quote', 200);
					$table->string('quote_by', 200);
					$table->string('content', 5000);
					$table->string('button_link', 100);
					$table->string('button_text', 50);
					$table->string('image_path', 200);
					$table->string('image_alt', 200);

					$table->string('class_names', 200);

					$table->boolean('enabled')->default(1);
					$table->boolean('global')->default(0);

					$table->timestamps();
					$table->softDeletes();

					$table->index('enabled');
					$table->index('global');

				}
			);
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\Schema::dropIfExists(static::$table);
	}

}
