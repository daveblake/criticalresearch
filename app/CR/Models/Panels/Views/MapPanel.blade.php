<div class="carousel">
  <ul>
  </ul>
</div>
<div class="cycle-slideshow"
     data-cycle-fx="scrollHorz"
    >
  <!-- empty element for pager links -->
  <div class="cycle-pager"></div>


  <img src="/img/carousel/team_1.jpg" alt="Team 1"/>
  <img src="/img/carousel/team_2.jpg" alt="Team 2"/>
  <img src="/img/carousel/team_3.jpg" alt="Team 3"/>
  <img src="/img/carousel/team_4.jpg" alt="Team 4"/>
  <img src="/img/carousel/team_5.jpg" alt="Team 5"/>
  <img src="/img/carousel/team_6.jpg" alt="Team 6"/>
  <img src="/img/carousel/team_7.jpg" alt="Team 7"/>
  <img src="/img/carousel/team_8.jpg" alt="Team 8"/>
  <img src="/img/carousel/team_9.jpg" alt="Team 9"/>
  <img src="/img/carousel/team_10.jpg" alt="Team 10"/>
</div>
<a id="contactus"></a>
<section id="{{{$Panel->uniqueId}}}" class="panel maps cf">
  <div class="mapBox">
    <div class="contact cf">
      <h3>Critical Luton</h3>

      <p class="address">
        Critical House,<br/>
        41-43 Alma Street,<br/>
        Luton<br/>
        LU1 2PL
      </p>

      <div class="info">
        <p class="tel">01582 480588</p>

        <a target="_blank"
           href="http://maps.google.co.uk/maps?f=q&hl=en&geocode=&q=LU1+2PL&ie=UTF8&z=16&iwloc=addr"
           class="mapLink">Map</a>
      </div>
    </div>
  </div>
  <div class="mapBox">

    <div class="contact cf">
      <h3>Critical Watford</h3>

      <p class="address">
        Suite F3,<br/>
        Hartsbourne House,<br/>
        Delta Gain,<br/>
        Carpenders Park,<br/>
        Watford<br/>
        WD19 5EF
      </p>

      <div class="info">
        <p class="tel">020 8421 6600</p>

        <a target="_blank"
           href="http://maps.google.co.uk/maps?f=q&hl=en&geocode=&q=WD19+5EF&ie=UTF8&z=16&iwloc=addr"
           class="mapLink">Map</a>
      </div>
    </div>
  </div>
  <div class="mapBox">

    <div class="contact cf">
      <h3>Critical London</h3>

      <p class="address">
        Unit 7,<br/>
        Baden Place,<br/>
        Crosby Row,<br/>
        London,<br/>
        SE1 1YW
      </p>

      <div class="info">
        <p class="tel">0203 643 9050</p>

        <a target="_blank"
           href="https://www.google.co.uk/maps/place/London+SE1+1YW/@51.5017265,-0.0892317,17z/data=!3m1!4b1!4m2!3m1!1s0x487603597688f33b:0x15bbb0a6c62dd908"
           class="mapLink">Map</a>
      </div>
    </div>
  </div>

</section>