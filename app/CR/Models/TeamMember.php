<?php

namespace CR\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class TeamMember extends \Eloquent
{

  use SoftDeletingTrait;

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'team_members';

  protected $fillable = [
    'name',
    'slug',
    'job_title',
    'quote',
    'description',
    'image_path',
    'featured',
    'weight'
  ];

  protected $guarded = ['id'];

  protected $dates = ['deleted_at'];

  public function firstName()
  {
    $nameParts = explode(' ', $this->name);
    return isset($nameParts[0]) ? $nameParts[0] : 'Team Member';
  }
}
