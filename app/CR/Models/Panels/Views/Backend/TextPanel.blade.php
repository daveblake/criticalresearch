<div class="adminPanel">
  <h2>Text Panel</h2>

  <input type="hidden"
         name="panels[{{$Panel->id}}][id]"
         value="{{$Panel->id}}"/>
  <input type="hidden"
         name="panels[{{$Panel->id}}][type]"
         value="{{$Panel->getType()}}"/>

  <div class="form-group">
    <label for="panel_title_{{$Panel->id}}">Panel Title</label>
    <input type="text" class="form-control"
           name="panels[{{$Panel->id}}][title]"
           id="panel_title{{$Panel->id}}" placeholder="Panel title"
           value="{{$Panel->title}}">
  </div>

  <div class="form-group">
    <label for="panel_content_{{$Panel->id}}">Panel Content</label>
    <textarea class="form-control ckeditor"
              name="panels[{{$Panel->id}}][content]"
              rows="3" id="panel_content_{{$Panel->id}}"
              placeholder="Panel Content">{{$Panel->content}}</textarea>
  </div>
</div>