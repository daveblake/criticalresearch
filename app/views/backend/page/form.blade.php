@extends('backend.layouts.master')
@section('content')

  <h3>{{$formType}} {{$page->title}} Page</h3>

  @if(Session::has('success'))
    <div class="alert  alert-success">
      {{ Session::get('success') }}
    </div>
  @endif

  @if ($errors->count())
    <div class="alert  alert-danger">
      <ul>
        {{ implode('', $errors->all('<li>:message</li>')) }}
      </ul>
    </div>
  @endif

  <form action="" method="post" enctype="multipart/form-data">


    @foreach($page->panels() as $Panel)
      {{$Panel->renderAdmin()}}
    @endforeach


    <h2>Seo Content</h2>

    <div class="form-group">
      <label for="meta_title">Meta Title</label>
      <input name="meta_title" type="text" class="form-control" id="meta_title"
             placeholder="Meta title" value="{{$page->meta_title}}">
    </div>

    <div class="form-group">
      <label for="meta_description">Meta Description</label>
      <textarea name="meta_description" class="form-control" rows="3"
                id="meta_description"
                placeholder="Meta Description">{{$page->meta_description}}</textarea>
    </div>

    <button type="submit" class="btn btn-default">Save</button>


  </form>
  <script src="//cdn.ckeditor.com/4.5.4/full/ckeditor.js"></script>
@endsection