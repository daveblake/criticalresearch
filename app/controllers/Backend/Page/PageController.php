<?php
namespace Backend\Page;

use Backend\BackendBaseController;
use CR\Models\Page;
use CR\Models\Panels\BasePanel;
use Illuminate\Http\Request;

class PageController extends BackendBaseController
{
  public function getIndex()
  {
    $pages                 = Page::all()->toArray();
    $this->layout->content = \View::make('backend.page.list')
      ->with('pages', $pages);
  }

  public function getCreate()
  {
    return $this->_form();
  }

  public function getEdit($id)
  {

    $page = (new Page)->find($id);
    try
    {
      if(is_null($page))
      {
        throw new \Exception('Invalid Page id');
      }
      $this->_form($page);
    }
    catch(\Exception $e)
    {
      return \Redirect::to('page')
        ->withErrors(
          [
            'Page could not be found',
            $e->getMessage()
          ]
        );
    }
  }

  public function _form($page = null)
  {

    $formType = is_null($page) ? 'Add' : 'Edit';
    if(is_null($page))
    {
      $page = new Page;
    }

    $this->layout->content = \View::make('backend.page.form')
      ->with('page', $page)
      ->with('formType', $formType);
  }

  public function postCreate()
  {
    $page = (new Page);
    return $this->_save($page);
  }

  public function postEdit($id)
  {
    $page = (new Page)->find($id);
    try
    {
      if(is_null($page))
      {
        throw new \Exception('Invalid Page id');
      }
    }
    catch(\Exception $e)
    {
      return \Redirect::to('page')
        ->withErrors(
          [
            'Page could not be found',
            $e->getMessage()
          ]
        );
    }

    return $this->_save($page, 'page/edit/' . $page->id);
  }

  protected function _save($page, $redirect = 'page')
  {

    $request = Request::createFromGlobals();

    $page->meta_title       = $request->input('meta_title');
    $page->meta_description = $request->input('meta_description');

    $panels = $request->input('panels');

    $reflect        = new \ReflectionClass(BasePanel::class);
    $panelNamespace = $reflect->getNamespaceName();

    if(is_array($panels) && count($panels))
    {
      foreach($panels as $panelData)
      {
        $panelClass = $panelNamespace . '\\' . $panelData['type'];
        if(!class_exists($panelClass))
        {
          continue;
        }

        /** @var BasePanel $panel */
        $panel = $panelClass::find($panelData['id']);

        if(!in_array($page->id, $panel->getPageIds()))
        {
          throw new \Exception('Panel does not belong to page.');
        }

        // if ( $panel->)
        foreach($panelData as $key => $val)
        {
          if(in_array($key, ['id', 'type']))
          {
            continue;
          }
          $panel->$key = $val;
        }
        $panel->save();
      }
    }

    $page->save();

    return \Redirect::to($redirect)
      ->withSuccess('The page was saved successfully');
  }

  public function getDelete($id)
  {
    try
    {
      $page = (new Page)->find($id);
      if(is_null($page))
      {
        throw new \Exception('Invalid Page id');
      }
      $page->delete();

      return \Redirect::to('page')->withSuccess($page['name'] . ' Removed');
    }
    catch(\Exception $e)
    {
      return \Redirect::to('page')
        ->withErrors(
          [
            'Page could not be deleted',
            $e->getMessage()
          ]
        );
    }
  }


  protected function _setActive()
  {
    $this->_navigation['page']['active'] = true;
  }
}
