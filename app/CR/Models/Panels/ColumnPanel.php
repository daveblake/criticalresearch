<?php

namespace CR\Models\Panels;

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Illuminate\View\View;

class ColumnPanel extends BasePanel
{

  use SoftDeletingTrait;

  protected $_type = 'ColumnPanel';


  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'panels_columns';

  protected $fillable = [
    'uniqueId',
    'column_type',
    'title',
    'button_link',
    'button_text',
    'button_qualifier',
    'class_names',
    'enabled',
    'global'
  ];

  protected $guarded = ['id'];

  protected $dates = ['deleted_at'];

  public function columns()
  {
    return $this->hasMany('CR\Models\Panels\ColumnPanelContent', 'panels_columns_id');
  }

}
