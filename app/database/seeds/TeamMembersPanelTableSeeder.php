<?php

class TeamMembersPanelTableSeeder extends Seeder
{

  public function run()
  {
    $panels = [
      [
        'uniqueId'    => 'teamMember',
        'class_names' => 'whiteBackground',
        'enabled'     => 1,
        'global'      => 1
      ]
    ];

    DB::table('panels_team_members')->truncate();
    foreach($panels as $panel)
    {
      if (! isset($panel['uniqueId']) ) {
        $panel['uniqueId'] = uniqid();
      }
      \CR\Models\Panels\TeamMembersPanel::create($panel);
    }
  }
}

